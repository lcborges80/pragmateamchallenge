package com.pragmateam.pragmateamchallenge.service;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.pragmateam.pragmateamchallenge.R;
import com.pragmateam.pragmateamchallenge.model.Checkin;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class CheckinServiceTest {

    private Checkin createCheckin(String key, String placeId, boolean voted, int votes) {
        Checkin checkin = new Checkin();
        checkin.setKey(key);
        checkin.setPlaceId(placeId);
        checkin.setVoted(voted);
        checkin.setVotes(votes);
        return checkin;
    }

    @Test
    public void testShouldCalculateVotes() {
        // given:
        CheckinService.releaseInstance();
        CheckinService checkinService = CheckinService.getInstance(InstrumentationRegistry.getTargetContext());
        Checkin checkin1 = createCheckin("deviceId1_placeId1", "placeId1", true, 3);
        Checkin checkin2 = createCheckin("deviceId2_placeId1", "placeId1", true, 2);
        Checkin checkin3 = createCheckin("deviceId1_placeId2", "placeId2", false, 0);
        Checkin checkin4 = createCheckin("deviceId3_placeId3", "placeId3", true, 1);
        HashMap<String, Checkin> checkinsFromFirebase = new HashMap<>();
        checkinsFromFirebase.put("deviceId1_placeId1", checkin1);
        checkinsFromFirebase.put("deviceId2_placeId1", checkin2);
        checkinsFromFirebase.put("deviceId1_placeId2", checkin3);
        checkinsFromFirebase.put("deviceId3_placeId3", checkin4);
        checkinService.setCheckinsFromFirebase(checkinsFromFirebase);

        // when:
        checkinService.loadVotesByRestaurant();

        // then:
        Map<String, Integer> votesByRestaurant = checkinService.getVotesByRestaurant();
        assertEquals("The map should be two elements.", 2, votesByRestaurant.size());
        assertEquals("Place1 should be five votes.", 5, votesByRestaurant.get("placeId1").intValue());
        assertNull("Place2 should be zero votes.", votesByRestaurant.get("placeId2"));
        assertEquals("Place3 should be one vote.", 1, votesByRestaurant.get("placeId3").intValue());
    }

    @Test
    public void testShouldIncrementVote() {
        // given:
        CheckinService.releaseInstance();
        CheckinService checkinService = CheckinService.getInstance(InstrumentationRegistry.getTargetContext());
        Checkin checkin = createCheckin("deviceId1_placeId1", "placeId1", true, 3);
        HashMap<String, Checkin> checkinsFromFirebase = new HashMap<>();
        checkinsFromFirebase.put("deviceId1_placeId1", checkin);
        checkinService.setCheckinsFromFirebase(checkinsFromFirebase);
        checkinService.loadVotesByRestaurant();

        // when:
        int votes = checkinService.incrementVoteByPlace("placeId1");

        // when:
        assertEquals("The number of the votes should be four.", 4, checkinService.getVotesByRestaurant().get("placeId1").intValue());
    }

    @Test
    public void testShouldShowMessageWithouVotes() {
        // given:
        CheckinService.releaseInstance();
        CheckinService checkinService = CheckinService.getInstance(InstrumentationRegistry.getTargetContext());
        Checkin checkin = createCheckin("deviceId1_placeId1", "placeId1", true, 3);
        HashMap<String, Checkin> checkinsFromFirebase = new HashMap<>();
        checkinsFromFirebase.put("deviceId1_placeId1", checkin);
        checkinService.setCheckinsFromFirebase(checkinsFromFirebase);
        checkinService.loadVotesByRestaurant();

        // when:
        String votesMessage = checkinService.getVotesFromPlace("placeId2");

        //then:
        Context context = InstrumentationRegistry.getTargetContext();
        assertEquals("The message should be no votes", context.getResources().getString(R.string.restaurantWithoutVotes), votesMessage);
    }

    @Test
    public void testShouldShowMessageWithOneVote() {
        // given:
        CheckinService.releaseInstance();
        CheckinService checkinService = CheckinService.getInstance(InstrumentationRegistry.getTargetContext());
        Checkin checkin = createCheckin("deviceId1_placeId1", "placeId1", true, 1);
        HashMap<String, Checkin> checkinsFromFirebase = new HashMap<>();
        checkinsFromFirebase.put("deviceId1_placeId1", checkin);
        checkinService.setCheckinsFromFirebase(checkinsFromFirebase);
        checkinService.loadVotesByRestaurant();

        // when:
        String votesMessage = checkinService.getVotesFromPlace("placeId1");

        //then:
        Context context = InstrumentationRegistry.getTargetContext();
        assertEquals("The message should be one vote", "1 " + context.getResources().getString(R.string.vote), votesMessage);
    }

    @Test
    public void testShouldShowMessageWithManyVote() {
        // given:
        CheckinService.releaseInstance();
        CheckinService checkinService = CheckinService.getInstance(InstrumentationRegistry.getTargetContext());
        Checkin checkin = createCheckin("deviceId1_placeId1", "placeId1", true, 2);
        HashMap<String, Checkin> checkinsFromFirebase = new HashMap<>();
        checkinsFromFirebase.put("deviceId1_placeId1", checkin);
        checkinService.setCheckinsFromFirebase(checkinsFromFirebase);
        checkinService.loadVotesByRestaurant();

        // when:
        String votesMessage = checkinService.getVotesFromPlace("placeId1");

        //then:
        Context context = InstrumentationRegistry.getTargetContext();
        assertEquals("The message should be many vote", "2 " + context.getResources().getString(R.string.votes), votesMessage);
    }

    @Test
    public void testShouldVotedToday() {
        // given:
        CheckinService.releaseInstance();
        CheckinService checkinService = CheckinService.getInstance(InstrumentationRegistry.getTargetContext());
        Checkin checkin = createCheckin("deviceId1_placeId1", "placeId1", false, 0);
        checkinService.setVoteOnCheckinRestaurant(checkin, 1);

        // when:
        boolean alreadyVotedToday = checkinService.alreadyVotedToday(checkin);

        // then:
        assertTrue("Should be voted on today", alreadyVotedToday);
        assertTrue(checkin.isVoted());
        assertEquals(1, checkin.getVotes());
    }

    @Test
    public void testShouldVisitedToday() {
        // given:
        CheckinService.releaseInstance();
        CheckinService checkinService = CheckinService.getInstance(InstrumentationRegistry.getTargetContext());
        Checkin checkin = createCheckin("deviceId1_placeId1", "placeId1", false, 0);
        checkinService.setCheckinDate(checkin);

        // when:
        boolean alreadyVisitedToday = checkinService.alreadyVisitedToday(checkin);

        // then:
        assertTrue("Should be visited on today", alreadyVisitedToday);
        assertFalse(checkin.isVoted());
        assertEquals(0, checkin.getVotes());
    }


}