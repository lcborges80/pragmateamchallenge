package com.pragmateam.pragmateamchallenge;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class HungryAlarmReceiver extends BroadcastReceiver {

    private static final int NOTIFICATION_ID = 1;

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.hungry_icon)
                .setContentTitle(context.getResources().getString(R.string.pragmaTeamChallenge))
                .setContentText(context.getResources().getString(R.string.notificatioMessage));
        builder.setContentIntent(getNotificationAction(context));
        builder.setAutoCancel(true);
        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notification);
    }

    private PendingIntent getNotificationAction(Context context) {
        Intent intent = new Intent(context, BlankActivity.class);
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

}
