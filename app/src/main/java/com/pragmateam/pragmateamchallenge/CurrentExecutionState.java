package com.pragmateam.pragmateamchallenge;

import com.pragmateam.pragmateamchallenge.model.Restaurant;

public class CurrentExecutionState {

    private static CurrentExecutionState instance;
    private Restaurant selectedRestaurant;

    public static CurrentExecutionState getInstance() {
        if (instance == null) {
            instance = new CurrentExecutionState();
        }
        return instance;
    }

    public Restaurant getSelectedRestaurant() {
        return selectedRestaurant;
    }

    public void setSelectedRestaurant(Restaurant selectedRestaurant) {
        this.selectedRestaurant = selectedRestaurant;
    }

}
