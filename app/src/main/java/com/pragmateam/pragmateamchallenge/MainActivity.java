package com.pragmateam.pragmateamchallenge;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pragmateam.pragmateamchallenge.adapter.RestaurantsAdapter;
import com.pragmateam.pragmateamchallenge.model.Restaurant;
import com.pragmateam.pragmateamchallenge.service.AlarmService;
import com.pragmateam.pragmateamchallenge.service.CheckinService;
import com.pragmateam.pragmateamchallenge.service.GoogleApiService;
import com.pragmateam.pragmateamchallenge.service.PermissionsService;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private GoogleApiService googleApiService;
    private PermissionsService permissionsService;

    private LinearLayout mainMessageContainer;
    private TextView message;
    private RecyclerView nearbyRestaurants;
    private RestaurantsAdapter restaurantsAdapter;
    private ImageButton loadNearbyRestaurants;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        this.mainMessageContainer = (LinearLayout) findViewById(R.id.mainMessageContainer);
        this.message = (TextView) findViewById(R.id.message);
        this.nearbyRestaurants = (RecyclerView) findViewById(R.id.nearbyRestaurants);
        this.loadNearbyRestaurants = (ImageButton) findViewById(R.id.loadNearbyRestaurants);

        this.googleApiService = new GoogleApiService(this);
        this.permissionsService = new PermissionsService(this, this.googleApiService);

        loadInitialMessage();
        CheckinService.getInstance(this).loadCheckinData();
        new AlarmService().setHungryAlarm(this);
    }

    private void loadInitialMessage() {
        this.nearbyRestaurants.setVisibility(View.GONE);
        this.loadNearbyRestaurants.setVisibility(View.GONE);
        this.message.setText(getResources().getString(R.string.loadDataFromFirebase));
        this.mainMessageContainer.setVisibility(View.VISIBLE);
    }

    public void allowLoadNearbyRestaurants() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                message.setText(getResources().getString(R.string.initial_message));
                loadNearbyRestaurants.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.refresh:
                loadInitialMessage();
                CheckinService.releaseInstance();
                CheckinService.getInstance(this).loadCheckinData();
                break;

        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.googleApiService.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.googleApiService.disconnect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (this.restaurantsAdapter != null) {
            this.restaurantsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        this.permissionsService.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void getNearbyRestaurants(View view) {
        this.mainMessageContainer.setVisibility(View.GONE);
        this.permissionsService.requestPermissions();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        CheckinService.releaseInstance();
        this.finish();
    }

    public void showRestaurants(final List<Restaurant> restaurants) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                restaurantsAdapter = new RestaurantsAdapter(MainActivity.this, restaurants, googleApiService);
                nearbyRestaurants.setVisibility(View.VISIBLE);
                nearbyRestaurants.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                nearbyRestaurants.setAdapter(restaurantsAdapter);
            }
        });
    }

}
