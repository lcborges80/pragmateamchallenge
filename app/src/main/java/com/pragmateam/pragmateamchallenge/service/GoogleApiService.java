package com.pragmateam.pragmateamchallenge.service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.PlacePhotoResult;
import com.google.android.gms.location.places.Places;
import com.pragmateam.pragmateamchallenge.MainActivity;
import com.pragmateam.pragmateamchallenge.R;
import com.pragmateam.pragmateamchallenge.model.Restaurant;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class GoogleApiService implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private Activity context;
    private GoogleApiClient googleApiClient;
    private ProgressDialog progressDialog;

    public GoogleApiService(Activity context) {
        this.context = context;
        this.progressDialog = new ProgressDialog(this.context);
        this.progressDialog.setCancelable(false);
        GoogleApiClient.Builder builder = new GoogleApiClient.Builder(this.context);
        builder.addApi(Places.GEO_DATA_API);
        builder.addApi(Places.PLACE_DETECTION_API);
        builder.addConnectionCallbacks(this);
        builder.addOnConnectionFailedListener(this);
        this.googleApiClient = builder.build();
    }

    public void connect() {
        this.googleApiClient.connect();
    }

    public void disconnect() {
        this.googleApiClient.disconnect();
    }

    public void getPlacesNearbyMe() {
        showProcessingDialog();
        PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi.getCurrentPlace(this.googleApiClient, null);
        result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
            @Override
            public void onResult(PlaceLikelihoodBuffer likelyPlaces) {
                List<Restaurant> restaurants = new ArrayList<>();
                for (PlaceLikelihood placeLikelihood : likelyPlaces) {
                    Place place = placeLikelihood.getPlace();
                    if (placeIsRestaurant(place)) {
                        Restaurant restaurant = new Restaurant(place);
                        restaurants.add(restaurant);
                    }
                }
                likelyPlaces.release();
                dismissProcessingDialog();
                ((MainActivity) context).showRestaurants(restaurants);
            }
        });
    }

    private boolean placeIsRestaurant(Place place) {
        List<Integer> placeTypes = place.getPlaceTypes();
        if (placeTypes != null) {
            for (Integer type : placeTypes) {
                if (type.intValue() == Place.TYPE_RESTAURANT || type.intValue() == Place.TYPE_FOOD) {
                    return true;
                }
            }
        }
        return false;
    }

    private void showProcessingDialog() {
        this.progressDialog.setMessage(this.context.getResources().getString(R.string.loading));
        this.progressDialog.show();
    }

    private void dismissProcessingDialog() {
        this.progressDialog.dismiss();
    }

    public void loadImagePlace(final Restaurant restaurant, final CircleImageView imageView) {

        new AsyncTask<Void, Void, Bitmap>() {

            @Override
            protected Bitmap doInBackground(Void... voids) {
                Bitmap image = null;
                PendingResult<PlacePhotoMetadataResult> pendingResult = Places.GeoDataApi.getPlacePhotos(googleApiClient, restaurant.getId());
                PlacePhotoMetadataResult placePhotoMetadataResult = pendingResult.await();
                if (placePhotoMetadataResult != null && placePhotoMetadataResult.getStatus().isSuccess()) {
                    PlacePhotoMetadataBuffer placePhotoMetadataBuffer = placePhotoMetadataResult.getPhotoMetadata();
                    if (placePhotoMetadataBuffer.getCount() > 0) {
                        PlacePhotoMetadata placePhotoMetadata = placePhotoMetadataBuffer.get(0);
                        PendingResult<PlacePhotoResult> pendingResultPlacePhotoResult = placePhotoMetadata.getPhoto(googleApiClient);
                        PlacePhotoResult placePhotoResult = pendingResultPlacePhotoResult.await();
                        if (placePhotoResult != null) {
                            image = placePhotoResult.getBitmap();
                        }
                    }
                    placePhotoMetadataBuffer.release();
                }
                return image;
            }

            @Override
            protected void onPostExecute(Bitmap image) {
                if (image != null) {
                    imageView.setImageBitmap(image);
                    restaurant.setImage(image);
                }
            }

        }.execute();

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

}
