package com.pragmateam.pragmateamchallenge.service;

import android.content.Context;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pragmateam.pragmateamchallenge.MainActivity;
import com.pragmateam.pragmateamchallenge.R;
import com.pragmateam.pragmateamchallenge.Utils;
import com.pragmateam.pragmateamchallenge.model.Checkin;
import com.pragmateam.pragmateamchallenge.model.Restaurant;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class CheckinService {

    private static CheckinService instance;
    private static final String TABLE_NAME = "restaurantCheckin";
    private DatabaseReference databaseReference;
    private Context context;
    private Map<String, Checkin> checkinsFromFirebase;
    private Map<String, Integer> votesByRestaurant;

    public static CheckinService getInstance(Context context) {
        if (instance == null) {
            instance = new CheckinService(context);
        }
        return instance;
    }

    private CheckinService(Context context) {
        this.context = context;
        this.databaseReference = FirebaseDatabase.getInstance().getReference();
        this.checkinsFromFirebase = new HashMap<>();
        this.votesByRestaurant = new HashMap<>();
    }

    public static void releaseInstance() {
        instance = null;
    }

    public void loadCheckinData() {
        this.databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> mapOne = (HashMap<String, Object>) dataSnapshot.getValue();
                if (mapOne != null) {
                    HashMap<String, Object> mapTwo = (HashMap<String, Object>) mapOne.get("restaurantCheckin");
                    Set<String> set = mapTwo.keySet();
                    Iterator<String> iterator = set.iterator();
                    while (iterator.hasNext()) {
                        String key = iterator.next();
                        HashMap<String, Object> fields = (HashMap<String, Object>) mapTwo.get(key);
                        Checkin checkin = new Checkin();
                        checkin.setKey(getValue(fields.get("key")));
                        checkin.setCheckinDate(getValue(fields.get("checkinDate")));
                        checkin.setPlaceId(getValue(fields.get("placeId")));
                        checkin.setUser(getValue(fields.get("user")));
                        checkin.setVoteDate(getValue(fields.get("voteDate")));
                        checkin.setVoted((Boolean) fields.get("voted"));
                        checkin.setVotes(((Long) fields.get("votes")).intValue());
                        checkinsFromFirebase.put(key, checkin);
                    }
                }
                databaseReference.removeEventListener(this);
                loadVotesByRestaurant();
                ((MainActivity) context).allowLoadNearbyRestaurants();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        });
    }

    public void setCheckinsFromFirebase(HashMap<String, Checkin> checkinsFromFirebase) {
        this.checkinsFromFirebase = checkinsFromFirebase;
    }

    public Map<String, Integer> getVotesByRestaurant() {
        return this.votesByRestaurant;
    }

    public void loadVotesByRestaurant() {
        if (this.checkinsFromFirebase.size() > 0) {
            Set<String> set = this.checkinsFromFirebase.keySet();
            Iterator<String> iterator = set.iterator();
            while (iterator.hasNext()) {
                Checkin checkin = this.checkinsFromFirebase.get(iterator.next());
                if (checkin.isVoted()) {
                    String placeId = checkin.getPlaceId();
                    if (this.votesByRestaurant.containsKey(placeId)) {
                        addVotes(checkin);
                    } else {
                        this.votesByRestaurant.put(placeId, checkin.getVotes());
                    }
                }
            }
        }
    }

    public void addVotes(Checkin checkin) {
        int votes = this.votesByRestaurant.get(checkin.getPlaceId());
        votes += checkin.getVotes();
        this.votesByRestaurant.put(checkin.getPlaceId(), votes);
    }

    public int incrementVoteByPlace(String placeId) {
        int votes;
        if (this.votesByRestaurant.containsKey(placeId)) {
            votes = this.votesByRestaurant.get(placeId);
            votes++;
            this.votesByRestaurant.put(placeId, votes);
        } else {
            this.votesByRestaurant.put(placeId, 1);
            votes = 1;
        }
        return votes;
    }

    public String getVotesFromPlace(String placeId) {
        if (this.votesByRestaurant.containsKey(placeId)) {
            int votes = this.votesByRestaurant.get(placeId);
            if (votes == 1) {
                return votes + " " + this.context.getResources().getString(R.string.vote);
            }
            return votes + " " + this.context.getResources().getString(R.string.votes);
        }
        return this.context.getResources().getString(R.string.restaurantWithoutVotes);
    }

    public Checkin getRestaurantCheckinFromFirebase(String key) {
        return this.checkinsFromFirebase.get(key);
    }

    private String getValue(Object value) {
        if (value != null) {
            return String.valueOf(value);
        }
        return "";
    }

    public Checkin createCheckinOnRestaurant(Restaurant restaurant) {
        Checkin checkin = new Checkin();
        checkin.setUser(Utils.getInstance(this.context).getDeviceId());
        checkin.setPlaceId(restaurant.getId());
        checkin.setKey(generateKey(checkin.getPlaceId()));
        setCheckinDate(checkin);
        checkin.setVoted(false);
        checkin.setVoteDate("");
        return checkin;
    }

    public void setCheckinDate(Checkin checkin) {
        checkin.setCheckinDate(String.valueOf(new Date().getTime()));
    }

    public void setVoteOnCheckinRestaurant(Checkin checkin, int votes) {
        checkin.setVoted(true);
        checkin.setVoteDate(String.valueOf(new Date().getTime()));
        checkin.setVotes(votes);
    }

    public void storeCheckinOnRestaurant(Checkin checkin) {
        this.databaseReference.child(TABLE_NAME).child(checkin.getKey()).setValue(checkin);
    }

    public boolean alreadyVotedToday(Checkin checkin) {
        if (checkin != null) {
            return Utils.getInstance(this.context).dateIsToday(checkin.getVoteDate());
        }
        return false;
    }

    public boolean alreadyVisitedToday(Checkin checkin) {
        if (checkin != null) {
            return Utils.getInstance(this.context).dateIsToday(checkin.getCheckinDate());
        }
        return false;
    }

    public boolean restaurantVisitedInTheSameWeek(Checkin checkin) {
        if (checkin != null) {
            return Utils.getInstance(this.context).isSameWeek(checkin.getCheckinDate());
        }
        return false;
    }

    public String generateKey(String placeId) {
        return Utils.getInstance(this.context).getDeviceId() + "_" + placeId;
    }

    public void doVote(Restaurant restaurant, Checkin checkin) {
        int votes = incrementVoteByPlace(checkin.getPlaceId());
        setVoteOnCheckinRestaurant(checkin, votes);
        storeCheckinOnRestaurant(checkin);
        restaurant.setCheckin(checkin);
    }

    public Checkin doCheckin(Restaurant restaurant, Checkin checkin) {
        if (checkin != null) {
            setCheckinDate(checkin);
        } else {
            checkin = createCheckinOnRestaurant(restaurant);
            restaurant.setCheckin(checkin);
        }
        storeCheckinOnRestaurant(checkin);
        return checkin;
    }

}