package com.pragmateam.pragmateamchallenge.service;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class PermissionsService {

    private static final int PERMISSIONS_REQUEST_CODE = 1;
    private Activity activity;
    private GoogleApiService googleApiService;

    public PermissionsService(Activity activity, GoogleApiService googleApiService) {
        this.activity = activity;
        this.googleApiService = googleApiService;
    }

    private boolean isMarshmallowOrHigherVersion() {
        return android.os.Build.VERSION.SDK_INT >= 23;
    }

    public void requestPermissions() {
        if (isMarshmallowOrHigherVersion()) {
            List<String> deniedDangerousPermissions = new ArrayList<>();
            if (ContextCompat.checkSelfPermission(this.activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                deniedDangerousPermissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (ContextCompat.checkSelfPermission(this.activity, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED) {
                deniedDangerousPermissions.add(Manifest.permission.READ_PHONE_STATE);
            }
            if (deniedDangerousPermissions.isEmpty()) {
                this.googleApiService.getPlacesNearbyMe();
            } else {
                String[] deniedDangerousPermissionsAsStringArray = new String[deniedDangerousPermissions.size()];
                for (int i = 0; i < deniedDangerousPermissions.size(); i++) {
                    deniedDangerousPermissionsAsStringArray[i] = deniedDangerousPermissions.get(i);
                }
                ActivityCompat.requestPermissions(this.activity, deniedDangerousPermissionsAsStringArray, PERMISSIONS_REQUEST_CODE);
            }
        } else {
            this.googleApiService.getPlacesNearbyMe();
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            List<String> dangerousPermissionsDeniedByUser = new ArrayList<>();
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    dangerousPermissionsDeniedByUser.add(permissions[i]);
                }
            }
            if (dangerousPermissionsDeniedByUser.isEmpty()) {
                this.googleApiService.getPlacesNearbyMe();
            } else {
                requestPermissions();
            }
        }
    }

}
