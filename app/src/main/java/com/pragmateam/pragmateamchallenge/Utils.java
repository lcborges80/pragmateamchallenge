package com.pragmateam.pragmateamchallenge;

import android.content.Context;
import android.graphics.Bitmap;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class Utils {

    private static Utils instance;
    private String deviceId;

    public static Utils getInstance(Context context) {
        if (instance == null) {
            instance = new Utils(context);
        }
        return instance;
    }

    private Utils(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        this.deviceId = telephonyManager.getDeviceId();
    }

    public String getDeviceId() {
        return this.deviceId;
    }

    public String formattedDate(String time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(time));
        StringBuilder formattedDate = new StringBuilder();
        formattedDate.append(calendar.get(Calendar.MONTH) + 1);
        formattedDate.append('/');
        formattedDate.append(calendar.get(Calendar.DAY_OF_MONTH));
        formattedDate.append('/');
        formattedDate.append(calendar.get(Calendar.YEAR));
        return formattedDate.toString();
    }

    public byte[] getImageAsBytes(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                byteArrayOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean dateIsToday(String time) {
        if (TextUtils.isEmpty(time)) {
            return false;
        }
        Calendar todayCalendar = Calendar.getInstance();
        todayCalendar.set(Calendar.HOUR, 0);
        todayCalendar.set(Calendar.HOUR_OF_DAY, 0);
        todayCalendar.set(Calendar.MINUTE, 0);
        todayCalendar.set(Calendar.SECOND, 0);
        todayCalendar.set(Calendar.MILLISECOND, 0);
        long todayTime = todayCalendar.getTimeInMillis();

        Calendar votedCalendar = Calendar.getInstance();
        votedCalendar.setTimeInMillis(Long.parseLong(time));
        votedCalendar.set(Calendar.HOUR, 0);
        votedCalendar.set(Calendar.HOUR_OF_DAY, 0);
        votedCalendar.set(Calendar.MINUTE, 0);
        votedCalendar.set(Calendar.SECOND, 0);
        votedCalendar.set(Calendar.MILLISECOND, 0);
        long votedTime = votedCalendar.getTimeInMillis();

        return todayTime == votedTime;
    }

    public boolean isSameWeek(String time) {
        if (TextUtils.isEmpty(time)) {
            return false;
        }
        Calendar weekCalendar = Calendar.getInstance();
        int currentWeek = weekCalendar.get(Calendar.WEEK_OF_YEAR);

        Calendar checkinCalendar = Calendar.getInstance();
        checkinCalendar.setTimeInMillis(Long.parseLong(time));
        int checkinWeek = checkinCalendar.get(Calendar.WEEK_OF_YEAR);

        return currentWeek == checkinWeek;
    }

}
