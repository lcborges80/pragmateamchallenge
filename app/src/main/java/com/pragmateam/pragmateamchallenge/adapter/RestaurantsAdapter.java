package com.pragmateam.pragmateamchallenge.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pragmateam.pragmateamchallenge.CurrentExecutionState;
import com.pragmateam.pragmateamchallenge.R;
import com.pragmateam.pragmateamchallenge.RestaurantDetails;
import com.pragmateam.pragmateamchallenge.Utils;
import com.pragmateam.pragmateamchallenge.model.Restaurant;
import com.pragmateam.pragmateamchallenge.model.Checkin;
import com.pragmateam.pragmateamchallenge.service.GoogleApiService;
import com.pragmateam.pragmateamchallenge.service.CheckinService;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RestaurantsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Restaurant> restaurants;
    private GoogleApiService googleApiService;
    private CheckinService checkinService;

    public RestaurantsAdapter(Context context, List<Restaurant> restaurants, GoogleApiService googleApiService) {
        this.context = context;
        this.restaurants = restaurants;
        this.googleApiService = googleApiService;
        this.checkinService = CheckinService.getInstance(this.context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderRestaurant(LayoutInflater.from(parent.getContext()).inflate(R.layout.restaurant_card, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ViewHolderRestaurant viewHolderRestaurant = (ViewHolderRestaurant) holder;
        final Restaurant restaurant = this.restaurants.get(position);
        setData(restaurant, viewHolderRestaurant);
        loadRestaurantImage(restaurant, viewHolderRestaurant);
        viewHolderRestaurant.mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openRestaurantDetails(restaurant);
            }
        });
    }

    private void loadCheckinFromFirebase(Restaurant restaurant) {
        if (restaurant.getCheckin() == null) {
            String key = this.checkinService.generateKey(restaurant.getId());
            restaurant.setCheckin(this.checkinService.getRestaurantCheckinFromFirebase(key));
        }
    }

    private void setData(Restaurant restaurant, ViewHolderRestaurant viewHolderRestaurant) {
        viewHolderRestaurant.placeName.setText(restaurant.getName());
        viewHolderRestaurant.placeVotes.setText(this.checkinService.getVotesFromPlace(restaurant.getId()));

        loadCheckinFromFirebase(restaurant);
        Checkin checkin = restaurant.getCheckin();

        if (checkin != null) {
            viewHolderRestaurant.lastVisit.setText(this.context.getResources().getString(R.string.lastCheckin) + ' ' + Utils.getInstance(this.context).formattedDate(checkin.getCheckinDate()));
            if (checkinService.alreadyVotedToday(checkin)) {
                viewHolderRestaurant.votedToday.setText(context.getResources().getString(R.string.restaurantAlreadyVoted));
            } else {
                viewHolderRestaurant.votedToday.setText("");
            }
            if (checkinService.restaurantVisitedInTheSameWeek(checkin)) {
                viewHolderRestaurant.visitedThisWeek.setText(context.getResources().getString(R.string.restaurantInTheSameWeek));
            } else {
                viewHolderRestaurant.visitedThisWeek.setText("");
            }
        } else {
            viewHolderRestaurant.lastVisit.setText("");
            viewHolderRestaurant.votedToday.setText("");
            viewHolderRestaurant.visitedThisWeek.setText("");
        }
    }

    private void openRestaurantDetails(Restaurant restaurant) {
        CurrentExecutionState.getInstance().setSelectedRestaurant(restaurant);
        Intent intent = new Intent(this.context, RestaurantDetails.class);
        intent.putExtra(RestaurantDetails.PLACE_NAME, restaurant.getName());
        intent.putExtra(RestaurantDetails.PLACE_PHONE_NUMBER, restaurant.getPhoneNumber());
        intent.putExtra(RestaurantDetails.PLACE_WEBSITE, restaurant.getWebsite());
        intent.putExtra(RestaurantDetails.CHECKIN, restaurant.getCheckin());
        this.context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return this.restaurants.size();
    }

    private void loadRestaurantImage(Restaurant restaurant, ViewHolderRestaurant viewHolderRestaurant) {
        if (restaurant.getImage() != null) {
            viewHolderRestaurant.placeImage.setImageBitmap(restaurant.getImage());
        } else {
            viewHolderRestaurant.placeImage.setImageResource(R.drawable.no_image_icon);
            this.googleApiService.loadImagePlace(restaurant, viewHolderRestaurant.placeImage);
        }
    }

    public class ViewHolderRestaurant extends RecyclerView.ViewHolder {

        final View mainView;
        final CircleImageView placeImage;
        final TextView placeName;
        final TextView lastVisit;
        final TextView placeVotes;
        final TextView votedToday;
        final TextView visitedThisWeek;


        public ViewHolderRestaurant(View view) {
            super(view);
            mainView = view;
            placeImage = (CircleImageView) view.findViewById(R.id.placeImage);
            placeName = (TextView) view.findViewById(R.id.placeName);
            lastVisit = (TextView) view.findViewById(R.id.lastVisit);
            placeVotes = (TextView) view.findViewById(R.id.placeVotes);
            votedToday = (TextView) view.findViewById(R.id.votedToday);
            visitedThisWeek = (TextView) view.findViewById(R.id.visitedThisWeek);
        }

    }

}