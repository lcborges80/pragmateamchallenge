package com.pragmateam.pragmateamchallenge;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.pragmateam.pragmateamchallenge.model.Checkin;
import com.pragmateam.pragmateamchallenge.model.Restaurant;
import com.pragmateam.pragmateamchallenge.service.CheckinService;

import de.hdodenhof.circleimageview.CircleImageView;

public class RestaurantDetails extends AppCompatActivity {

    public static final String PLACE_NAME = "placeName";
    public static final String PLACE_PHONE_NUMBER = "placePhoneNumber";
    public static final String PLACE_WEBSITE = "placeWebsite";
    public static final String CHECKIN = "checkin";

    private CircleImageView placeImage;
    private TextView placeName;
    private TextView placePhoneNumber;
    private TextView placeWebsite;
    private ToggleButton addVote;
    private ToggleButton doCheckin;
    private Checkin checkin;
    private CheckinService checkinService;
    private Restaurant restaurant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restaurant_details);
        this.checkinService = CheckinService.getInstance(this);
        this.placeImage = (CircleImageView) findViewById(R.id.placeImage);
        this.placeName = (TextView) findViewById(R.id.placeName);
        this.placePhoneNumber = (TextView) findViewById(R.id.placePhoneNumber);
        this.placeWebsite = (TextView) findViewById(R.id.placeWebsite);
        this.doCheckin = (ToggleButton) findViewById(R.id.checkin);
        this.addVote = (ToggleButton) findViewById(R.id.vote);
        this.restaurant = CurrentExecutionState.getInstance().getSelectedRestaurant();

        this.doCheckin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    checkin = checkinService.doCheckin(restaurant, checkin);
                    doCheckin.setEnabled(false);
                    doCheckin.setChecked(false);
                }
            }
        });

        this.addVote.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    if (checkinService.alreadyVisitedToday(checkin)) {
                        checkinService.doVote(restaurant, checkin);
                        addVote.setEnabled(false);
                        addVote.setChecked(false);
                        addVote.setTextOff(getResources().getString(R.string.restaurantVoted));
                    } else {
                        addVote.setChecked(false);
                        Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.cantVote), Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
        });

        loadRestaurantDetails();
        allowCheckin();
        allowVote();
    }

    private void loadRestaurantDetails() {
        Intent intent = getIntent();
        Bitmap image = this.restaurant.getImage();
        if (image != null) {
            this.placeImage.setImageBitmap(image);
        } else {
            this.placeImage.setImageResource(R.drawable.no_image_icon);
        }
        this.placeName.setText(intent.getStringExtra(PLACE_NAME));
        this.placePhoneNumber.setText(intent.getStringExtra(PLACE_PHONE_NUMBER));
        this.placeWebsite.setText(intent.getStringExtra(PLACE_WEBSITE));
        this.checkin = (Checkin) intent.getSerializableExtra(CHECKIN);
    }

    private void allowCheckin() {
        if (this.checkin != null) {
            boolean restaurantAlreadyVisitedInThisWeek = this.checkinService.restaurantVisitedInTheSameWeek(this.checkin);
            if (restaurantAlreadyVisitedInThisWeek) {
                this.doCheckin.setEnabled(false);
                this.doCheckin.setText(getResources().getString(R.string.restaurantInTheSameWeek));
            }
        }
    }

    private void allowVote() {
        if (this.checkin != null) {
            boolean alreadyVotedToday = this.checkinService.alreadyVotedToday(this.checkin);
            if (alreadyVotedToday) {
                this.addVote.setEnabled(false);
                this.addVote.setText(getResources().getString(R.string.restaurantAlreadyVoted));
            }
        }
    }

}
