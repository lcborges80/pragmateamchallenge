package com.pragmateam.pragmateamchallenge.model;

import android.graphics.Bitmap;

import com.google.android.gms.location.places.Place;

public class Restaurant {

    private String id;
    private Bitmap image;
    private String name;
    private String phoneNumber;
    private String website;
    private Checkin checkin;

    public Restaurant(Place place) {
        this.id = place.getId();
        this.name = place.getName() != null ? place.getName().toString() : "";
        this.phoneNumber = place.getPhoneNumber() != null ? place.getPhoneNumber().toString() : "";
        this.website = place.getWebsiteUri() != null ? place.getWebsiteUri().toString() : "";
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Checkin getCheckin() {
        return checkin;
    }

    public void setCheckin(Checkin checkin) {
        this.checkin = checkin;
    }

}
