# README #
This repo contains the project "Pragma Challenge Team". This project is an Android application that implements some User Stories about list of restaurants, checkin e restaurants votes control.

The following technologies were used:
- Android as Platform;
- Firebase as Cloud Database;
- Google Places API for Android as Places Provider.

This app only has two activities: one for show the button that starts the nearby restaurants query and show the list of restaurants and another for show the restaurant details and do checkin and to vote. This second activity has the rules that control the unique checkin per weekend by restaurant and one vote per day per restaurant.

The user is controlled by deviceId, so each device is a unique user. 
The data storage is a single model that represents a checkin. The checkin key is DEVICEID and PLACEID. PLACEID represents the id from Place object, returned by Google Places API. The Checkin model has informations about your date and votes by placeId and deviceId.
The sum of votes is calculated by Android application at runtime.

Each restaurant loads, from Google Places API, your own image asynchronously.

The alarm is triggered at 12:55 PM and shows a notification with an action that shows a blank activity.

The list of restaurants shows the restaurants's name, last checkin date, restaurant votes, if restaurant already voted today and if restaurant have been visited on the current week.

To see the changes on records, use two (or more) devices, do checkins and votes and reload the list throught the "reload" menu.

The following point could be better:
- The filter by "restaurants" and "food" is being made on returned list that contains all places types. I tried use PlaceFilter class, but, with filter, the API doesn't returned any results.
- The notification action could call an Activity on Stack Activities.
- The interface elements received the MINIMAL treatment. Majority of drawables not are in correct resources folder by resolution.
- The app not controls network erros. (the connections callbacks were not implemented).

The app can be downloaded from https://goo.gl/Pm0raJ
(Please, adjust your Android settings for allow app download out of Google Play)

Thank you for taking part in this challenge!